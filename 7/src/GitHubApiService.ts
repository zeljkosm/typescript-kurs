import * as request from "request";
import { User } from "./User";
import { Repo } from "./Repo";

export class GitHubApiService {

    getUserInfo(userName: string, cb: (user: User) => any) {
        const options: any = {
            headers: {
                "User-Agent": "request"
            }
        };

        request.get("https://api.github.com/users/" + userName, options, (error: any, response: any, body: any) => {
            const user = new User(JSON.parse(body));
            cb(user);
        });
    }

    getRepos(userName: string, cb: (repos: Repo[]) => any) {
        const options: any = {
            headers: {
                "User-Agent": "request",
            }/*,
            json: true*/
        };
        request.get("https://api.github.com/users/" + userName + "/repos", options, (error: any, response: any, body: any) => {
            // console.log(body);
            body = JSON.parse(body);
            const repos = body.map((repo: any) => new Repo(repo));
            cb(repos);
        });
    }

}
