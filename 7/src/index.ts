console.log("Cao");

import { GitHubApiService } from "./GitHubApiService";
import { User } from "./User";
import { Repo } from "./Repo";
import * as _ from "lodash";

const svc = new GitHubApiService();

/*
svc.getUserInfo("typescript", (user: User) => {
    console.log(user);
});

svc.getRepos("typescript", (repos: Repo[]) => {
    console.log(repos);
});
*/

if (process.argv.length < 3) {
    console.log("Please pass the username as an argument");
} else {
    const username = process.argv[2];
    svc.getUserInfo(username, (user: User) => {
        svc.getRepos(username, (repos: Repo[]) => {
            const sortedRepos = _.sortBy(repos, [(repo: Repo) => repo.forkCount * -1]);
            const filteredRepos = _.take(sortedRepos, 5);
            user.repos = filteredRepos;
            console.log(user);
        });
    });
}

