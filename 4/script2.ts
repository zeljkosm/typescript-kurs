const niz01: number[] = [1, 2, 43];
const niz02: number[] = [4, 6, 88];

const funkcija5 = (niz1: number[], niz2: number[]): void => {
    const spojeniNiz1: number[] = [...niz1, ...niz2];
    console.log(spojeniNiz1);
    const spojeniNiz2: number[] = [...niz2, ...niz1];
    console.log(spojeniNiz2);
};

console.log("ZADATAK 2");
funkcija5([...niz01], [...niz02]);
