let a: string;
let b: number;
let c: boolean;

function funkcija(tekst: string, broj: number, bool: boolean): void {
    if (bool) {
        console.log(tekst + broj);
    } else {
        console.log("Boolean vrednost je false");
    }
}

a = "test";
b = 5;
c = false;

console.log("ZADATAK 1");
funkcija(a, b, c);
