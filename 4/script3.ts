interface IHrana {
    naziv: string;
    kalorije: number;
}

const funkcija3 = (obj: IHrana): void => {
    console.log(obj.naziv + " " + obj.kalorije);
};

let hrana: IHrana = {
    naziv: "Hleb",
    kalorije: 300
};

console.log("ZADATAK 3");
funkcija3(hrana);

