export class Meni {

    jela: string[];
    strane: number;

    constructor(jela: string[], strane: number) {
        this.jela = jela;
        this.strane = strane;
    }

    ispisi() {
        console.log("Meni sadrzi: ");
        for (let i = 0; i < this.jela.length; i++) {
            console.log(this.jela[i]);
        }
    }

}



const noviMeni = new Meni(["pasulj", "sarma", "pica"], 2);
console.log("ZADATAK 4");
noviMeni.ispisi();
