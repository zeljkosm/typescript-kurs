"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var script4_1 = require("./script4");
var DnevniMeni = /** @class */ (function (_super) {
    __extends(DnevniMeni, _super);
    function DnevniMeni() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DnevniMeni.prototype.ispisi = function () {
        console.log("Dnevni meni sadrzi: ");
        for (var i = 0; i < this.jela.length; i++) {
            console.log(this.jela[i]);
        }
    };
    return DnevniMeni;
}(script4_1.Meni));
var noviDnevniMeni = new DnevniMeni(["pasulj", "boranija", "burek"], 1);
console.log("ZADATAK 5");
noviDnevniMeni.ispisi();
//# sourceMappingURL=script5.js.map