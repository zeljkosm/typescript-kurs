"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Meni = /** @class */ (function () {
    function Meni(jela, strane) {
        this.jela = jela;
        this.strane = strane;
    }
    Meni.prototype.ispisi = function () {
        console.log("Meni sadrzi: ");
        for (var i = 0; i < this.jela.length; i++) {
            console.log(this.jela[i]);
        }
    };
    return Meni;
}());
exports.Meni = Meni;
var noviMeni = new Meni(["pasulj", "sarma", "pica"], 2);
console.log("ZADATAK 4");
noviMeni.ispisi();
//# sourceMappingURL=script4.js.map