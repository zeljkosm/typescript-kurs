import { Meni } from './script4';

class DnevniMeni extends Meni {

    ispisi() {
        console.log("Dnevni meni sadrzi: ");
        for (let i = 0; i < this.jela.length; i++) {
            console.log(this.jela[i]);
        }
    }

}

const noviDnevniMeni = new DnevniMeni(["pasulj", "boranija", "burek"], 1);
console.log("ZADATAK 5");
noviDnevniMeni.ispisi();
