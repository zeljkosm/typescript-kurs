export interface Ispit {
    brojIndeksa: number;
    ime: string;
    prezime: string;
    sifraPredmeta: number;
    nazivPredmeta: string;

    ispisiPodatke(): void;
}
