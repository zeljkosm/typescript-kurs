import { IspitniRok } from "./classes/IspitniRok";

let ispitniRokovi: IspitniRok[];

const ispitniRok1 = new IspitniRok(1111, "Pera", "Peric", 1, "Osnove programiranja");
const ispitniRok2 = new IspitniRok(1212, "Branka", "Beric", 2, "Fizika");
const ispitniRok3 = new IspitniRok(1311, "Zoran", "Zoric", 2, "Fizika");

ispitniRokovi = [ispitniRok1, ispitniRok2, ispitniRok3];

/*
const brojIndeksaInput = <HTMLInputElement>document.getElementById("brojIndeksa");
const imeInput = <HTMLInputElement>document.getElementById("ime");
const prezimeInput = <HTMLInputElement>document.getElementById("prezime");
const sifraPredmetaInput = <HTMLInputElement>document.getElementById("sifraPredmeta");
const nazivPredmetaInput = <HTMLInputElement>document.getElementById("nazivPredmeta");

const brojIndeksa = parseInt(brojIndeksaInput.value, 10);
const ime = imeInput.value;
const prezime = prezimeInput.value;
const sifraPredmeta = parseInt(sifraPredmetaInput.value, 10);
const nazivPredmeta = nazivPredmetaInput.value;
*/

function unesiIspitniRok(brojIndeksa: number, ime: string, prezime: string, sifraPredmeta: number, nazivPredmeta: string): void {
    const novi = new IspitniRok(brojIndeksa, ime, prezime, sifraPredmeta, nazivPredmeta);
    ispitniRokovi.push(novi);
}

function obrisiIspitniRok(ime: string, prezime: string): void {
    ispitniRokovi = ispitniRokovi.filter(
        item => item.ime !== ime || item.prezime !== prezime
    );
}

function izmeniIspitniRok(brojIndeksa: number, ime: string, prezime: string, sifraPredmeta: number, nazivPredmeta: string): void {
    const match: IspitniRok[] = ispitniRokovi.filter(
        item => item.ime === ime && item.prezime === prezime
    );
    if (match) {
        match.forEach(function(rok: IspitniRok) {
            rok.ime = ime;
            rok.prezime = prezime;
            rok.brojIndeksa = brojIndeksa;
            rok.sifraPredmeta = sifraPredmeta;
            rok.nazivPredmeta = nazivPredmeta;
        });
    }
}

function prikaziIspitniRok(ime: string, prezime: string): void {
    ispitniRokovi = ispitniRokovi.filter(
        item => item.ime === ime || item.prezime === prezime
    );
    console.log(ispitniRokovi);
}

function ispisiIspitniRok(): void {
    console.log(ispitniRokovi);
}

ispisiIspitniRok();

console.log("-------------------");

unesiIspitniRok(4444, "Nikola", "Nikolic", 4, "Matematika");

ispisiIspitniRok();

console.log("-------------------");

obrisiIspitniRok("Zoran", "Zoric");

ispisiIspitniRok();

console.log("-------------------");

izmeniIspitniRok(3333, "Nikola", "Nikolic", 2, "Fizika");

ispisiIspitniRok();

console.log("-------------------");

prikaziIspitniRok("Branka", "Beric");
