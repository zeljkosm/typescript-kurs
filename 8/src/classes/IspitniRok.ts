import { Ispit } from "../interfaces/Ispit";

export class IspitniRok implements Ispit {
    brojIndeksa: number;
    ime: string;
    prezime: string;
    sifraPredmeta: number;
    nazivPredmeta: string;

    constructor(brojIndeksa: number, ime: string, prezime: string, sifraPredmeta: number, nazivPredmeta: string) {
        this.brojIndeksa = brojIndeksa;
        this.ime = ime;
        this.prezime = prezime;
        this.sifraPredmeta = sifraPredmeta;
        this.nazivPredmeta = nazivPredmeta;
    }

    ispisiPodatke(): void {
        console.log("Broj indeksa: " + this.brojIndeksa);
        console.log("Ime: " + this.ime);
        console.log("Prezime: " + this.prezime);
        console.log("Sifra predmeta: " + this.sifraPredmeta);
        console.log("Naziv predmeta: " + this.nazivPredmeta);
    }
}
