"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var IspitniRok = /** @class */ (function () {
    function IspitniRok(brojIndeksa, ime, prezime, sifraPredmeta, nazivPredmeta) {
        this.brojIndeksa = brojIndeksa;
        this.ime = ime;
        this.prezime = prezime;
        this.sifraPredmeta = sifraPredmeta;
        this.nazivPredmeta = nazivPredmeta;
    }
    IspitniRok.prototype.ispisiPodatke = function () {
        console.log("Broj indeksa: " + this.brojIndeksa);
        console.log("Ime: " + this.ime);
        console.log("Prezime: " + this.prezime);
        console.log("Sifra predmeta: " + this.sifraPredmeta);
        console.log("Naziv predmeta: " + this.nazivPredmeta);
    };
    return IspitniRok;
}());
exports.IspitniRok = IspitniRok;

//# sourceMappingURL=IspitniRok.js.map
