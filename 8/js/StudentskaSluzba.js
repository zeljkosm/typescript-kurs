"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var IspitniRok_1 = require("./classes/IspitniRok");
var ispitniRokovi;
var ispitniRok1 = new IspitniRok_1.IspitniRok(1111, "Pera", "Peric", 1, "Osnove programiranja");
var ispitniRok2 = new IspitniRok_1.IspitniRok(1212, "Branka", "Beric", 2, "Fizika");
var ispitniRok3 = new IspitniRok_1.IspitniRok(1311, "Zoran", "Zoric", 2, "Fizika");
ispitniRokovi = [ispitniRok1, ispitniRok2, ispitniRok3];
/*
const brojIndeksaInput = <HTMLInputElement>document.getElementById("brojIndeksa");
const imeInput = <HTMLInputElement>document.getElementById("ime");
const prezimeInput = <HTMLInputElement>document.getElementById("prezime");
const sifraPredmetaInput = <HTMLInputElement>document.getElementById("sifraPredmeta");
const nazivPredmetaInput = <HTMLInputElement>document.getElementById("nazivPredmeta");

const brojIndeksa = parseInt(brojIndeksaInput.value, 10);
const ime = imeInput.value;
const prezime = prezimeInput.value;
const sifraPredmeta = parseInt(sifraPredmetaInput.value, 10);
const nazivPredmeta = nazivPredmetaInput.value;
*/
function unesiIspitniRok(brojIndeksa, ime, prezime, sifraPredmeta, nazivPredmeta) {
    var novi = new IspitniRok_1.IspitniRok(brojIndeksa, ime, prezime, sifraPredmeta, nazivPredmeta);
    ispitniRokovi.push(novi);
}
function obrisiIspitniRok(ime, prezime) {
    ispitniRokovi = ispitniRokovi.filter(function (item) { return item.ime !== ime || item.prezime !== prezime; });
}
function izmeniIspitniRok(brojIndeksa, ime, prezime, sifraPredmeta, nazivPredmeta) {
    var match = ispitniRokovi.filter(function (item) { return item.ime === ime || item.prezime === prezime; });
    if (match) {
        match.forEach(function (rok) {
            rok.ime = ime;
            rok.prezime = prezime;
            rok.brojIndeksa = brojIndeksa;
            rok.sifraPredmeta = sifraPredmeta;
            rok.nazivPredmeta = nazivPredmeta;
        });
    }
}
function prikaziIspitniRok(ime, prezime) {
    ispitniRokovi = ispitniRokovi.filter(function (item) { return item.ime === ime || item.prezime === prezime; });
    console.log(ispitniRokovi);
}
function ispisiIspitniRok() {
    console.log(ispitniRokovi);
}
ispisiIspitniRok();
console.log("-------------------");
unesiIspitniRok(4444, "Nikola", "Nikolic", 4, "Matematika");
ispisiIspitniRok();
console.log("-------------------");
obrisiIspitniRok("Zoran", "Zoric");
ispisiIspitniRok();
console.log("-------------------");
izmeniIspitniRok(3333, "Nikola", "Nikolic", 2, "Fizika");
ispisiIspitniRok();
console.log("-------------------");
prikaziIspitniRok("Branka", "Beric");

//# sourceMappingURL=StudentskaSluzba.js.map
