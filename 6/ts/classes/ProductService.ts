import * as fromInterface from './../interfaces/index';

export class ProductService implements fromInterface.IProductService {

    private products: fromInterface.IProduct[];
    private categories: fromInterface.ICategory[];

    constructor(products: fromInterface.IProduct[], categories: fromInterface.ICategory[]) {
        this.products = products;
        this.categories = categories;
    }

    getCategories(): fromInterface.ICategory[] {
        return this.categories;
    }

    getAllProducts(): fromInterface.IProduct[] {
        return this.products;
    }

    getProductsByCategory(categoryId: number): fromInterface.IProduct[] {
        const filteredProducts: fromInterface.IProduct[] = this.products.filter(product => product.category === categoryId);
        return filteredProducts;
    }

    addProduct(product: fromInterface.IProduct): void {
        this.products.push(product);
    }

    addRating(productId: number, rating: fromInterface.IRating): void {
        const productIndex = this.products.findIndex(product => product.id === productId);
        const updatedProduct = {...this.products[productIndex]};
        if (updatedProduct.ratings) {
            updatedProduct.ratings.push(rating);
        } else {
            const ratings: fromInterface.IRating[] = [];
            updatedProduct.ratings = ratings;
            updatedProduct.ratings.push(rating);
        }
        this.products.splice(productIndex, 1);
        this.products.push(updatedProduct);
    }

}
