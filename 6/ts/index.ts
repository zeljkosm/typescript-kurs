import { ProductService } from "./classes/ProductService";
import { CATEGORIES, PRODUCTS } from "./data";

const service = new ProductService(PRODUCTS, CATEGORIES);

console.log("Categories: " + JSON.stringify(service.getCategories()));
console.log("Products: " + JSON.stringify(service.getAllProducts()));
console.log("Filtered products TEST2: " + JSON.stringify(service.getProductsByCategory(1)));
service.addRating(1, { id: 4, comment: "ok ok"});
console.log("Products after adding rating: " + JSON.stringify(service.getAllProducts()));

