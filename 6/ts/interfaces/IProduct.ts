import { IRating } from "./IRating";

export interface IProduct {

    id: number;
    name: string;
    originalPrice: number;
    offerPrice?: number;
    stock: number;
    category: number;
    ratings?: IRating[];

}
