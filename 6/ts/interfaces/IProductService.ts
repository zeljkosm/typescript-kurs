import { ICategory } from "./ICategory";
import { IProduct } from "./IProduct";
import { IRating } from "./IRating";

export interface IProductService {

    getCategories(): ICategory[];

    getAllProducts(): IProduct[];

    getProductsByCategory(categoryId: number): IProduct[];

    addProduct(product: IProduct): void;

    addRating(productId: number, rating: IRating): void;

}
