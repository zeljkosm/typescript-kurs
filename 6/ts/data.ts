import * as fromInterface from './interfaces/index';

const CATEGORIES: fromInterface.ICategory[] = [
    {
        id: 1,
        name: "MOBILES2"
    },
    {
        id: 2,
        name: "BAKERY"
    },
    {
        id: 3,
        name: "FRUITS"
    }
];

const PRODUCTS: fromInterface.IProduct[] = [
    {
        id: 1,
        name: "iphone",
        originalPrice: 599,
        stock: 20,
        category: 1,
        ratings: [
            {
                id: 1,
                comment: "Good phone"
            },
            {
                id: 2,
                comment: "Awesome"
            },
            {
                id: 3,
                comment: "Good but pricy"
            }
        ]
    },
    {
        id: 2,
        name: "bread",
        originalPrice: 10,
        stock: 20,
        category: 2,
        ratings: [
            {
                id: 1,
                comment: "fresh and Yummy"
            },
            {
                id: 2,
                comment: "good taste"
            }
        ]
    }
];

export { CATEGORIES, PRODUCTS };
