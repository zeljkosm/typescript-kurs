"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var ProductService = /** @class */ (function () {
    function ProductService(products, categories) {
        this.products = products;
        this.categories = categories;
    }
    ProductService.prototype.getCategories = function () {
        return this.categories;
    };
    ProductService.prototype.getAllProducts = function () {
        return this.products;
    };
    ProductService.prototype.getProductsByCategory = function (categoryId) {
        var filteredProducts = this.products.filter(function (product) { return product.category === categoryId; });
        return filteredProducts;
    };
    ProductService.prototype.addProduct = function (product) {
        this.products.push(product);
    };
    ProductService.prototype.addRating = function (productId, rating) {
        var productIndex = this.products.findIndex(function (product) { return product.id === productId; });
        var updatedProduct = __assign({}, this.products[productIndex]);
        if (updatedProduct.ratings) {
            updatedProduct.ratings.push(rating);
        }
        else {
            var ratings = [];
            updatedProduct.ratings = ratings;
            updatedProduct.ratings.push(rating);
        }
        this.products.splice(productIndex, 1);
        this.products.push(updatedProduct);
    };
    return ProductService;
}());
exports.ProductService = ProductService;

//# sourceMappingURL=ProductService.js.map
