"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ProductService_1 = require("./classes/ProductService");
var data_1 = require("./data");
var service = new ProductService_1.ProductService(data_1.PRODUCTS, data_1.CATEGORIES);
console.log("Categories: " + JSON.stringify(service.getCategories()));
console.log("Products: " + JSON.stringify(service.getAllProducts()));
console.log("Filtered products TEST2: " + JSON.stringify(service.getProductsByCategory(1)));
service.addRating(1, { id: 4, comment: "ok ok" });
console.log("Products after adding rating: " + JSON.stringify(service.getAllProducts()));

//# sourceMappingURL=index.js.map
