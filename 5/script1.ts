class Animal {

    name: string;

    constructor(name: string) {
        this.name = name;
    }

    move(distanceInMeters: number = 0): void {
        console.log(this.name + " moved " + distanceInMeters + " meters");
    }

    behave(trait: string): void {
        console.log(this.name + " does a " + trait);
    }

}

class Snake extends Animal {

    constructor(name: string) {
        super(name);
    }

    move(distanceInMeters: number = 5): void {
        console.log("My Snake is Slithering...");
        super.move(distanceInMeters);
    }

    behave(trait: string = "hiss"): void {
        console.log("Hissing SCARILY!!...");
        super.behave(trait);
    }

}

class Horse extends Animal {

    constructor(name: string) {
        super(name);
    }

    move(distanceInMeters: number = 45): void {
        console.log("That Horse is Galloping...");
        super.move(distanceInMeters);
    }

}

console.log("----------------------");
console.log("TASK 1");

let sammy = new Snake("Sammy the Python");
let tommy = new Horse("Tommy the Horse");
sammy.behave();
tommy.move(34);

