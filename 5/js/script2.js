"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Department = /** @class */ (function () {
    function Department(name) {
        this.name = name;
        this.printName();
    }
    Department.prototype.printName = function () {
        console.log("Department is " + this.name);
    };
    return Department;
}());
var AccountingDepartment = /** @class */ (function (_super) {
    __extends(AccountingDepartment, _super);
    function AccountingDepartment() {
        return _super.call(this, "Accounting Department") || this;
    }
    AccountingDepartment.prototype.printMeeting = function (subDept) {
        var message = "You have a meeting with the " + subDept + " department at 08:00 IST";
        console.log(message);
        return message;
    };
    return AccountingDepartment;
}(Department));
var BioTechnologyDepartment = /** @class */ (function (_super) {
    __extends(BioTechnologyDepartment, _super);
    function BioTechnologyDepartment() {
        return _super.call(this, "BioTechnology and Genetics Department") || this;
    }
    BioTechnologyDepartment.prototype.printMeeting = function (subDept) {
        var message = "You have a meeting with the " + subDept + " department at 14:00 IST";
        console.log(message);
        return message;
    };
    return BioTechnologyDepartment;
}(Department));
console.log("----------------------");
console.log("TASK 2");
var accountingDep = new AccountingDepartment();
var bioTechDep = new BioTechnologyDepartment();
var message1 = accountingDep.printMeeting("Finance");
var message2 = bioTechDep.printMeeting("Cellbiology");
var button = document.getElementById("btn");
var appointments = document.getElementById("appointments");
if (button && appointments) {
    button.addEventListener("click", function () {
        appointments.innerHTML = message1 + "<br>" + message2;
    });
}
