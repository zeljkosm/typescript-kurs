abstract class Department {

    name: string;

    constructor(name: string) {
        this.name = name;
        this.printName();
    }

    printName(): void {
        console.log("Department is " + this.name);
    }

    abstract printMeeting(subDept: string): void;

}

class AccountingDepartment extends Department {

    constructor() {
        super("Accounting Department");
    }

    printMeeting(subDept: string): string {
        const message = "You have a meeting with the " + subDept + " department at 08:00 IST";
        console.log(message);
        return message;
    }

}

class BioTechnologyDepartment extends Department {

    constructor() {
        super("BioTechnology and Genetics Department");
    }

    printMeeting(subDept: string): string {
        const message = "You have a meeting with the " + subDept + " department at 14:00 IST";
        console.log(message);
        return message;
    }

}

console.log("----------------------");
console.log("TASK 2");

const accountingDep = new AccountingDepartment();
const bioTechDep = new BioTechnologyDepartment();

const message1 = accountingDep.printMeeting("Finance");
const message2 = bioTechDep.printMeeting("Cellbiology");

const button = document.getElementById("btn");
const appointments = document.getElementById("appointments");

if (button && appointments) {
    button.addEventListener("click", function() {
        appointments.innerHTML = message1 + "<br>" + message2;
    });
}




